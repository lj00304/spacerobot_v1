# **Space Robot Simulator V1**
Space robot simulator that replicates the motion of a 4/5/6 dof free flying orbital space robot, similar to that discussed 
in [1,2]. The motion is gouverned by the dynamics outlined in [2]. This simulator is used in paper [3] presented at IROS 2021.
The package follows the structre of the OpenAI gym envs [4], with the following funcitonality:
```
env.reset() #resets env to initial state this includes the position of the payload and the position of the space robot.
next_state, reward, done, if_captured = env.step(action) #takes a step in the environment under the given action.
env.render() #renders env in the current state
```
 NOTE: for render to work you must first call `env.initialise_render()` prior to the first `env.reset()`

## Installation
Installation should be done by cloning this repo. Note that if this env is being used in conjunction with [3] then a version is included in the corresponding repo.
You can install libraries using pip install -r requirements.txt. We suggest using a virtualenv in order to prevent conflicts. An example of this:

               conda create -n <ENVNAME> python=3.9

               conda activate <ENVNAME>

               pip install -r requirements.txt

## Observation Space
The observation space has a dimension of 22+2*dof, dependent of the selected dof of the arm, where each element is as follows:

| Num | Observation | Min | Max |
| ------ | ------ | ----- | ----- |
| [0:6] | Base Position | -10 | 10 |
| [6:6+dof] | Arm Position | -0.6 | 0.6 |
| [6+dof:12+dof] | Base Velocity | -5 | 5 |
| [12+dof:12+(2xdof)] | Arm Velocity | -5 | 5 |
| [12+(2xdof):18+(2xdof)] | Payload Location | -10 | 10 |
| [18+(2xdof):21+(2xdof)] | Payload Size | -5 | 5 |
| [21+(2xdof):22+(2xdof)] | Payload Mass | 0 | 100 |

## Action Space
The action space has a dimension of 6 + dof, where each element is as follows:

| Num | Action |
| ------ | ------ |
| [0:3] | Linear base force |
| [3:6] | Angluar base force |
| [6:6+dof] | Angluar force on each arm joint|

## Reward
This environment uses a sparse reward based on the final state of the environment.

| Reward | Final State |
| ------ | ------ |
| -velocity of arm and base | aims to encourage controlled movement |
| -10 | if joint limits are exceeded |
| -50 | if the satellite collides |
| +40 | for payload capture with high base velocity |
| +100 | for payload capture with low base velocity |

Episode Termination occurs for any of the following:
1.     Payload capture
2.     Satellite collision
3.     Joint limits exceeded (+/- 0.6 rad)

## Example Code

```
from space_robot.envs import *

env = SpaceRobotEnv() # can use env = SpaceRobotEnv(dof=5) to change dof - default is 4
obs = env.reset()
env.initialise_render() # needed if rendering is wanted
done = False
while not done:
        action = env.action_space.sample()
        new_obs, reward, done, captured = env.step(action)
        env.render()
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

# References
- [1] Lucy Jackson, Chakravarthini M. Saaj, Asma Seddaoui, Calem Whiting, Steve Eckersley, Simon Hadfield, "Downsizing an orbital space robot: A dynamic system based evaluation", Advances in Space Research, Volume 65, Issue 10, 2020, Pages 2247-2262, ISSN 0273-1177, https://doi.org/10.1016/j.asr.2020.03.004. (https://www.sciencedirect.com/science/article/pii/S0273117720301435)
- [2] Seddaoui, Asma & Saaj, Chakravarthini. (2019). H∞ CONTROL FOR A CONTROLLED FLOATING ROBOTIC SPACECRAFT. (https://www.semanticscholar.org/paper/H-INFINITY-CONTROL-FOR-A-CONTROLLED-FLOATING-Seddaoui-Saaj/eec198f187d832904d8b5a9f2b9e62789713ca08)
- [3] Lucy Jackson, Steve Eckersley, Pete Senior, Simon Hadfield, "HARL-A: Hardware Agnostic Reinforcement Learning Through Adversarial Selection", IROS (2021), 27th Oct. - 1st Nov 2021 (https://www.researchgate.net/publication/353347347_HARL-A_Hardware_Agnostic_Reinforcement_Learning_Through_Adversarial_Selection)
- [4] https://gym.openai.com/

